# TP1 B2 RESEAU

## I. Exploration locale en solo

### 1. Affichage d'informations sur la pile TCP/IP locale*

#### Affichez les infos des cartes réseau de votre PC

```
C:\Users\guigu> ipconfig /all```
[...]
Carte réseau sans fil Wi-Fi : 

Adresse physique . . . . . . . . . . . : D8-12-65-B5-11-77
Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.93(préféré)
[...]
``` 
#### Affichez les infos des cartes réseau de votre PC

``` 
Carte réseau sans fil Wi-Fi :
[...]
Passerelle par défaut. . . . . . . . . : 10.33.3.253
``` 

#### Trouvez comment afficher les informations sur une carte IP (change selon l'OS)

![](img/img_01.png)

#### à quoi sert la gateway dans le réseau d'YNOV ?

- Elle sert a passer du réseau local de YNOV a un autre réseau

### 2. Modifications des informations

---

### A. Modification d'adresse IP (part 1)

![](img/img_02.png)

#### Il est possible que vous perdiez l'accès internet. Que ce soit le cas ou non, expliquez pourquoi c'est possible de perdre son accès internet en faisant cette opération:

- Car on peut tomber sur une IP déjà utilisée et entrer en conflit avec cette IP.

### B. Table ARP

#### Exploration de la table ARP

```
C:\Users\guigu>arp -a

Interface : 192.168.56.1 --- 0xc
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.2.97 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.33.0.54            d0-3c-1f-fd-6f-88     dynamique
[...]
```
l'adresse MAC de la passerelle est trouvable car on connaît son adresse IP, il reste plus qu'à regarder dans la table ARP.
```
[...]
10.33.3.253           00-12-00-40-4c-bf     dynamique
[...]
```

#### Et si on remplissait un peu la table ?

``` 
C:\Users\guigu>ping 10.33.1.104

Envoi d’une requête 'Ping'  10.33.1.104 avec 32 octets de données :
Réponse de 10.33.1.104 : octets=32 temps=158 ms TTL=64
[...]

C:\Users\guigu>ping 10.33.2.11

Envoi d’une requête 'Ping'  10.33.2.11 avec 32 octets de données :
Réponse de 10.33.2.11 : octets=32 temps=12 ms TTL=128
[...]

C:\Users\guigu>ping 10.33.2.167

Envoi d’une requête 'Ping'  10.33.2.167 avec 32 octets de données :
Réponse de 10.33.2.167 : octets=32 temps=12 ms TTL=128
[...]
``` 

``` 
C:\Users\guigu>arp -a
[...]
Interface : 10.33.2.97 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.33.1.104           a0-78-17-67-3a-d4     dynamique
  10.33.2.11            80-32-53-7e-ae-64     dynamique
  10.33.2.167           58-96-1d-14-ca-7b     dynamique
```

### C. nmap

#### Utilisez nmap pour scanner le réseau de votre carte WiFi et trouver une adresse IP libre

``` 
C:\Users\guigu>nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 16:58 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.10
Host is up (0.22s latency).
MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
Nmap scan report for 10.33.0.20
Host is up (0.0040s latency).
MAC Address: 74:29:AF:33:1B:69 (Hon Hai Precision Ind.)
Nmap scan report for 10.33.0.21
Host is up (0.0050s latency).
MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
Nmap scan report for 10.33.0.27
Host is up (0.0040s latency).
[...]
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.2.93
Host is up.
Nmap done: 1024 IP addresses (185 hosts up) scanned in 38.89 seconds
``` 

- Afficher la table ARP

``` 
C:\Users\guigu>arp -a

Interface : 192.168.56.1 --- 0x8
  Adresse Internet      Adresse physique      Type
  192.168.56.255        ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique

Interface : 10.33.2.93 --- 0x26
  Adresse Internet      Adresse physique      Type
  10.33.0.3             22-93-d2-7e-9f-1a     dynamique
  10.33.0.174           40-ec-99-c6-dc-c5     dynamique
  10.33.0.244           ce-3d-70-5e-c8-db     dynamique
  [...]
``` 
### D. Modification d'adresse IP (part 2)

``` 
C:\Users\guigu>nmap -sP 10.33.0.0/22
Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 17:04 Paris, Madrid (heure dÆÚtÚ)
Nmap scan report for 10.33.0.10
Host is up (0.33s latency).
MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
Nmap scan report for 10.33.0.20
Host is up (0.0050s latency).
MAC Address: 74:29:AF:33:1B:69 (Hon Hai Precision Ind.)
Nmap scan report for 10.33.0.21
Host is up (0.011s latency).
[...]
MAC Address: 00:0E:C4:CD:74:F5 (Iskra Transmission d.d.)
Nmap scan report for 10.33.0.24
Host is up.
Nmap done: 1024 IP addresses (186 hosts up) scanned in 27.05 seconds
``` 

## II. Exploration locale en duo

### 3. Modification d'adresse IP

#### Modifier l'IP des deux machines pour qu'elles soient dans le même réseau
Nous avons défini nos IP sur 192.168.0.1 et 192.168.0.2, avec un masque en /30 pour avoir 2 IP disponibles.

#### Vérifier à l'aide de commandes que les changements ont pris effet
```
$ yoshi@yoshi:~$ ip a | grep enp3s0
2: enp3s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 192.168.0.1/30 brd 192.168.0.3 scope global noprefixroute enp3s0
    
$ ipconfig
Carte Ethernet Ethernet :
    Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2
```
#### Utiliser ping pour tester la connectivité entre les deux machines
```
$ ping 192.168.0.1

Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=64
Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=64

Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 3, reçus = 3, perdus = 0 (perte 0%),
Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 1ms, Moyenne = 1ms

$ yoshi@yoshi:~$ ping 192.168.0.2

PING 192.168.0.2 (192.168.0.2) 56(84) bytes of data.
64 bytes from 192.168.0.2: icmp_seq=1 ttl=128 time=1.27 ms
64 bytes from 192.168.0.2: icmp_seq=2 ttl=128 time=1.12 ms
64 bytes from 192.168.0.2: icmp_seq=3 ttl=128 time=1.19 ms
^C
--- 192.168.0.2 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.118/1.192/1.271/0.062 ms
```
#### Afficher et consulter la table ARP

```
$ arp -a
Interface : 192.168.0.2 --- 0x3
  Adresse Internet      Adresse physique      Type
  192.168.0.1           00-d8-61-e8-40-99     dynamique
  [...]
```

### 4. Utilisation d'un des deux comme gateway
C'est la carte réseau de mon PC qui servait de Gateway, voici le côté client qui passe par mon IP ( 192.168.0.2 )

#### ** Pour tester la connectivité à internet on fait souvent des requêtes simples vers un serveur internet connu**
- encore une fois, un ping vers un DNS connu comme 1.1.1.1 ou 8.8.8.8 c’est parfait

```
$ ping 1.1.1.1
PING 1.1.1.1 (1.1.1.1) 56(84) bytes of data.
64 bytes from 1.1.1.1: icmp_seq=1 ttl=57 time=19.7 ms
64 bytes from 1.1.1.1: icmp_seq=2 ttl=57 time=20.1 ms
64 bytes from 1.1.1.1: icmp_seq=3 ttl=57 time=20.5 ms
--- 1.1.1.1 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 19.705/20.107/20.514/0.330 ms

$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=21.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=21.4 ms
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 20.169/20.985/21.412/0.577 ms
```

#### ** Utiliser un traceroute ou tracert pour bien voir que les requêtes passent par la passerelle choisie (l'autre le PC)**

```
$ traceroute -4 8.8.8.8
traceroute to 8.8.8.8 (8.8.8.8), 30 hops max, 60 byte packets
 1  _gateway (192.168.0.2)  1.194 ms  1.129 ms  1.098 ms
 2  * * *
 3  10.33.3.253 (10.33.3.253)  9.630 ms  9.604 ms  9.578 ms
 4  10.33.10.254 (10.33.10.254)  9.551 ms  13.057 ms  13.495 ms
 5  reverse.completel.net (92.103.174.137)  13.467 ms  13.440 ms  13.944 ms
 6  92.103.120.182 (92.103.120.182)  16.237 ms  10.069 ms  10.008 ms
 7  172.19.130.117 (172.19.130.117)  22.050 ms  20.905 ms  21.887 ms
 8  46.218.128.74 (46.218.128.74)  18.867 ms  18.841 ms  19.195 ms
 9  38.147.6.194.rev.sfr.net (194.6.147.38)  20.175 ms  20.149 ms  19.610 ms
10  72.14.194.30 (72.14.194.30)  19.094 ms  19.559 ms  20.065 ms
11  * * *
12  dns.google (8.8.8.8)  19.795 ms  19.780 ms  18.861 ms
```

### 5. Petit chat privé
#### ** Sur le PC serveur qui possède l'IP 192.168.0.2**

```
$ .\nc.exe -l -p 8888
Salut
Ca va
Yup
```
#### ** Sur le PC client qui possède l'IP 192.168.0.1**

```
$ nc 192.168.0.2 8888
Salut
Ca va
Yup
```

#### ** Pour aller un peu plus loin**
On se met en écoute seulement sur l'ip 192.168.0.1 et sur le port 9999

**Sur le PC serveur**
```
$ .\nc.exe -l -p 9999 192.168.0.1
Coucou
Ca marche
!
```
**Sur le PC client**
```
$ nc 192.168.0.2 9999
Coucou
Ca marche
!
```


## III. Manipulations d'autres outils/protocoles côté client

### 1. DHCP

#### Exploration du DHCP, depuis votre PC
- #### ![](img/img_03.png) (Il expire environ 2h après avoir été obtenu)

### 2. DNS
#### Trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
C:\Users\guigu> ipconfig /all

Configuration IP de Windows
[...]
Carte réseau sans fil Wi-Fi :
   Serveurs DNS. . .  . . . . . . . . . . : 2606:4700:4700::1111
                                       2606:4700:4700::1001
                                       1.1.1.1
                                       8.8.8.8
   NetBIOS sur Tcpip. . . . . . . . . . . : Activé

[...]
```
L'IP du DNS ici est donc 1.1.1.1 et 8.8.8.8 pour le secondaire

#### Lookup google.com
```
C:\Users\guigu> nslookup google.com
Serveur :   one.one.one.one
Address:  1.1.1.1

Réponse ne faisant pas autorité :
Nom :    google.com
Addresses:  2a00:1450:4007:810::200e
          216.58.214.174
```

#### Lookup ynov.com
```
C:\Users\guigu> nslookup ynov.com
Serveur :   one.one.one.one
Address:  1.1.1.1

Réponse ne faisant pas autorité :
DNS request timed out.
    timeout was 2 seconds.
Nom :    ynov.com
Address:  92.243.16.143
```
Ces commandes permettent de demander au serveur DNS l'IP correspondant au nom recherché, si il y a un résultat il l'envoie.

#### Déterminer l'adresse IP du serveur à qui vous venez d'effectuer ces requêtes

```
Serveur :   one.one.one.one
Address:  1.1.1.1
```
Les requêtes sont envoyées sur le serveur 1.1.1.1

#### Reverse lookup 78.74.21.21
```
C:\Users\guigu> nslookup 78.74.21.21
Serveur :   one.one.one.one
Address:  1.1.1.1

Nom :    host-78-74-21-21.homerun.telia.com
Address:  78.74.21.21
```
### Reverse lookup 92.146.54.88
```
C:\Users\guigu> nslookup 92.146.54.88
Serveur :   one.one.one.one
Address:  1.1.1.1

Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
Address:  92.146.54.88
```
### Interpréter les résultats
On peut donc retrouver a l'inverse, un nom à partir d'une IP en cherchant dans le DNS.
